package edu.ucr.cs.bdlab.jhdf;

import io.netty.buffer.ByteBuf;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class HDFFileTest extends TestHelper {
  public void testReadHeaderEntries() throws IOException {
    File hdfPath = duplicateResource("/MYD11A1.A2002185.h01v08.006.2015146150800.hdf");
    HDFFile hdfFile = new HDFFile(hdfPath.getPath());
    try {
      DDVGroup dataGroup = hdfFile.findGroupByName("LST_Day_1km");
      for (DataDescriptor dd : dataGroup.getContents()) {
        if (dd instanceof DDVDataHeader) {
          DDVDataHeader vheader = (DDVDataHeader) dd;
          if (vheader.getName().equals("scale_factor")) {
            Object unparsedValue = vheader.getEntryAt(0);
            assertEquals(Double.class, unparsedValue.getClass());
            assertEquals(0.02, (Double)unparsedValue, 1E-3);
          } else if (vheader.getName().equals("valid_range")) {
            Object unparsedValue = vheader.getEntryAt(0);
            assertEquals(7500, (int) (Integer) unparsedValue);
            unparsedValue = vheader.getEntryAt(1);
            assertEquals(65535, (int) (Integer) unparsedValue);
          } else if (vheader.getName().equals("_FillValue")) {
            Object unparsedValue = vheader.getEntryAt(0);
            assertEquals(0, (int) (Integer) unparsedValue);
          }
        }
      }
    } finally {
      hdfFile.close();
    }
  }

  public void testReadNDVI() throws IOException {
    File hdfPath = duplicateResource("/2016.03.05.h11v04.006.2016111112206.hdf");
    HDFFile hdfFile = new HDFFile(hdfPath.getPath());
    try {
      DDVGroup dataGroup = hdfFile.findGroupByName("1 km 16 days NDVI");
      for (DataDescriptor dd : dataGroup.getContents()) {
        if (dd instanceof DDVDataHeader) {
          DDVDataHeader vheader = (DDVDataHeader) dd;
          if (vheader.getName().equals("scale_factor")) {
            Object unparsedValue = vheader.getEntryAt(0);
            assertEquals(Double.class, unparsedValue.getClass());
            assertEquals(10000.0, (Double)unparsedValue, 1E-3);
          } else if (vheader.getName().equals("valid_range")) {
            Object unparsedValue = vheader.getEntryAt(0);
            assertEquals(-2000, (short) (Short) unparsedValue);
            unparsedValue = vheader.getEntryAt(1);
            assertEquals(10000, (short) (Short) unparsedValue);
          } else if (vheader.getName().equals("_FillValue")) {
            Object unparsedValue = vheader.getEntryAt(0);
            assertEquals(-3000, (short) (Short) unparsedValue);
          }
        } else if (dd instanceof DDNumericDataGroup) {
          DDNumericDataGroup numericDataGroup = (DDNumericDataGroup) dd;
          int valueSize = numericDataGroup.getDataSize();
          int resolution = numericDataGroup.getDimensions()[0];
          byte[] unparsedDataArray = new byte[valueSize * resolution * resolution];
          numericDataGroup.getAsByteArray(unparsedDataArray, 0, unparsedDataArray.length);
          ByteBuffer dataBuffer = ByteBuffer.wrap(unparsedDataArray);
          assertEquals(2345, dataBuffer.getShort(0));
          assertEquals(4139, dataBuffer.getShort(2 * 1200 * 1200 - 2));
        }
      }
    } finally {
      hdfFile.close();
    }
  }

  public void testReadFires() throws IOException {
    File hdfPath = duplicateResource("/2010.06.02.h08v05.WILDFIRE.hdf");
    HDFFile hdfFile = new HDFFile(hdfPath.getPath());
    try {
      DDVGroup dataGroup = hdfFile.findGroupByName("FireMask");
      for (DataDescriptor dd : dataGroup.getContents()) {
        if (dd instanceof DDVDataHeader) {
          DDVDataHeader vheader = (DDVDataHeader) dd;
          if (vheader.getName().equals("valid_range")) {
            Object unparsedValue = vheader.getEntryAt(0);
            assertEquals(0, (byte) (Byte) unparsedValue);
            unparsedValue = vheader.getEntryAt(1);
            assertEquals(9, (byte) (Byte) unparsedValue);
          } else if (vheader.getName().equals("_FillValue")) {
            Object unparsedValue = vheader.getEntryAt(0);
            assertEquals(0, (byte) (Byte) unparsedValue);
          }
        } else if (dd instanceof DDNumericDataGroup) {
          DDNumericDataGroup numericDataGroup = (DDNumericDataGroup) dd;
          int valueSize = numericDataGroup.getDataSize();
          int resolution = numericDataGroup.getDimensions()[0];
          byte[] unparsedDataArray = new byte[valueSize * resolution * resolution];
          numericDataGroup.getAsByteArray(unparsedDataArray, 0, unparsedDataArray.length);
          ByteBuffer dataBuffer = ByteBuffer.wrap(unparsedDataArray);
          assertEquals(4, dataBuffer.get(0));
          assertEquals(3, dataBuffer.get(1200));
          assertEquals(9, dataBuffer.get(1200 * 1200 - 1));
        }
      }
    } finally {
      hdfFile.close();
    }
  }
}