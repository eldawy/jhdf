/*
 * Copyright 2019 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.jhdf;

import junit.framework.TestCase;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.LineReader;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A base class for tests that adds some helper functions for copying test resources and for testing the results.
 */
public abstract class TestHelper extends TestCase {

  protected static final File scratchPath = new File("scratch_test");

  @Override
  protected void setUp() throws Exception {
    super.setUp();
    FileUtil.fullyDelete(scratchPath);
    scratchPath.mkdirs();
  }

  @Override
  protected void tearDown() throws Exception {
    super.tearDown();
    FileUtil.fullyDelete(scratchPath);
    if (scratchPath.exists())
      fail("Scratch directory not cleaned up correctly");
  }

  /**
   * Read a text file from the resources path as an array of lines.
   * @param resourcePath a path to a resource in the class path (or the JAR file)
   * @return an array of all lines in the file as a string
   * @throws IOException if the resource cannot be loaded
   */
  public static String[] readTextResource(String resourcePath) throws IOException {
    return readTextResource(resourcePath, Integer.MAX_VALUE);
  }

  /**
   * Read the first n lines from the given resource and return those lines as an array of Strings.
   * If the given upper bound is bigger than the input file, the entire input file is loaded and returned.
   * Therefore, the returned array might be smaller than the given upper bound if the file is smaller.
   * @param resourcePath the path to the resource to read
   * @param maxLines the upper bound of the number of lines to read
   * @return an array of strings containing the lines read from the input file
   * @throws IOException if the resource cannot be loaded or located.
   */
  public static String[] readTextResource(String resourcePath, int maxLines) throws IOException {
    InputStream is = null;
    try {
      is = new BufferedInputStream(TestHelper.class.getResourceAsStream(resourcePath));
      return getLines(is, maxLines);
    } finally {
      if (is != null)
        is.close();
    }
  }

  /**
   * Read a text file as a single big string.
   * @param filename the name (or path) of the file
   * @return the contents of the file as one big String.
   * @throws IOException if the file cannot be located or loaded.
   */
  public static String[] readFile(String filename) throws IOException {
    InputStream is = null;
    try {
      is = new BufferedInputStream(new FileInputStream(filename));
      return getLines(is, Integer.MAX_VALUE);
    } finally {
      if (is != null)
        is.close();
    }
  }

  /**
   * Reads up-to maxLines from the given input stream
   * @param is the input stream to read from
   * @param maxLines the upper bound of the number of lines to read
   * @return the lines as an array of Strings
   * @throws IOException if thrown by the underlying input stream.
   */
  private static String[] getLines(InputStream is, int maxLines) throws IOException {
    LineReader reader = new LineReader(is);
    Text line = new Text();
    List<String> lines = new ArrayList<String>();
    while (lines.size() < maxLines && reader.readLine(line) > 0)
      lines.add(line.toString());

    return lines.toArray(new String[lines.size()]);
  }

  /**
   * Reads a comma-separated-value (CSV) text file from the given path and returns all the values
   * as two dimensional array where the first index indicates the attribute number and the second index indicates
   * the line number.
   * @param resourcePath the path of the resource to read
   * @return a two-dimensional array of doubles that contain the parsed attributes.
   * @throws IOException if the resource cannot be located or loaded.
   */
  public static double[][] readCoordsResource(String resourcePath) throws IOException {
    return readCoordsResource(resourcePath, Integer.MAX_VALUE);
  }

  /**
   * Reads a CSV file that contains only numbers and returns the result as a two-dimensional array in a column format.
   * That is, if the input contains {@code n} lines and each line contains {@code d} columns, the return value is an
   * array of size {@code d} where each entry is an array of size {@code n}.
   * @param resourcePath
   * @param maxLines
   * @return
   * @throws IOException
   */
  public static double[][] readCoordsResource(String resourcePath, int maxLines) throws IOException {
    String[] lines = readTextResource(resourcePath, maxLines);
    int numDimensions = lines[0].split(",").length;
    double[][] coords = new double[numDimensions][lines.length];
    for (int i = 0; i < lines.length; i++) {
      String[] parts = lines[i].split(",");
      assert numDimensions == parts.length : String.format("Number of dimensions differ %d <> %d in line #%d",
          parts.length, numDimensions, i);
      for (int dim = 0; dim < numDimensions; dim++)
        coords[dim][i] = Double.parseDouble(parts[dim]);
    }
    return coords;
  }

  /**
   * Copy a resource from the class path to the given filepath.
   * @param resourcePath the path of the resource to read (can be inside the JAR file)
   * @param filePath the path of the file to write to.
   * @throws IOException if thrown while reading or writing.
   */
  public static void copyResource(String resourcePath, File filePath) throws IOException {
    copyResource(resourcePath, filePath, false);
  }

  /**
   * Copy a resource to a temporary file to allow reading it as a file.
   * @param resourcePath the path of the resource to read
   * @param filePath the path of the file to write
   * @param overwrite set this flag to automatically overwrite the output file.
   */
  public static void copyResource(String resourcePath, File filePath, boolean overwrite) throws IOException {
    if (!overwrite && filePath.exists())
      fail("Cannot overwrite an existing file " + filePath);
      // Create directory
    if (!new File(filePath.getParent()).exists())
      new File(filePath.getParent()).mkdirs();
    InputStream in = null;
    FileOutputStream out = null;
    try {
      byte[] buffer = new byte[1024 * 1024];
      in = TestHelper.class.getResourceAsStream(resourcePath);
      out = new FileOutputStream(filePath);
      int bufferLength;
      while ((bufferLength = in.read(buffer, 0, buffer.length)) > 0) {
        out.write(buffer, 0, bufferLength);
      }
    } finally {
      if (in != null)
        in.close();
      if (out != null)
        out.close();
    }
  }

  /**
   * Makes a copy of a resource into the scratch directory.
   * @param resourcePath
   * @return
   * @throws IOException
   */
  public static File duplicateResource(String resourcePath) throws IOException {
    File localFile = new File(scratchPath.toString(), new Path(resourcePath).getName());
    while (localFile.exists()) {
      int randomNumber = (int) (Math.random() * 1000000);
      localFile = new File(scratchPath, String.format("%06d_%s", randomNumber, new Path(resourcePath).getName()));
    }
    copyResource(resourcePath, localFile);
    return localFile;
  }


  /**
   * Copies an entire directory from the resource path to the given local directory.
   * @param resourcePath a path to a directory in the class path (can be inside the JAR file)
   * @param localDirPath a path to local directory to write to.
   */
  public static void copyDirectoryFromResources(String resourcePath, File localDirPath) throws IOException {
    if (!resourcePath.endsWith("/"))
      resourcePath += "/";
    BufferedReader inputFiles = new BufferedReader(new InputStreamReader(TestHelper.class.getResourceAsStream(resourcePath)));
    String file;
    if (!localDirPath.exists())
      localDirPath.mkdirs();
    while ((file = inputFiles.readLine()) != null) {
      copyResource(resourcePath + file, new File(localDirPath, file));
    }
  }

  /**
   * Read a resource as a raw byte array
   * @param resourcePath the path to the resource
   * @return the contents of that resource as a byte array
   * @throws IOException if encountered while reading or writing.
   */
  public static byte[] readResourceData(String resourcePath) throws IOException {
    InputStream in = TestHelper.class.getResourceAsStream(resourcePath);
    try {
      int totalSize = 0;
      List<byte[]> buffers = new ArrayList<byte[]>();
      boolean eofReached;
      do {
        byte[] buffer = new byte[4096];
        int bufferSize = in.read(buffer);
        if (bufferSize > 0) {
          if (bufferSize != buffer.length)
            buffer = Arrays.copyOf(buffer, bufferSize);
          buffers.add(buffer);
          totalSize += bufferSize;
        }
        eofReached = bufferSize == -1;
      } while (!eofReached);
      byte[] allData = new byte[totalSize];
      int offset = 0;
      for (byte[] buffer : buffers) {
        System.arraycopy(buffer, 0, allData, offset, buffer.length);
        offset += buffer.length;
      }
      assert offset == totalSize;
      return allData;
    } finally {
      in.close();
    }
  }

  /**
   * Tests the contents of two input stream for equality. The bytes are consumed from both streams while testing them.
   * @param expected the correct (expected) stream
   * @param actual the stream produced from the code
   * @throws IOException
   */
  public static void assertEquals(InputStream expected, InputStream actual) throws IOException {
    // The last offset that was compared
    long offsetCompared = 0;
    byte[] expectedBuffer = new byte[1024];
    byte[] actualBuffer = new byte[1024];
    int expectedLength = 0;
    int actualLength = 0;
    while (true) {
      int readLength = expected.read(expectedBuffer, expectedLength, expectedBuffer.length - expectedLength);
      if (readLength > 0)
        expectedLength += readLength;
      readLength = actual.read(actualBuffer, actualLength, actualBuffer.length - actualLength);
      if (readLength > 0)
        actualLength += readLength;
      if ((expectedLength == 0) ^ (actualLength == 0))
        fail("File lengths not equal!");
      if (expectedLength == 0 && actualLength == 0)
        return;
      for (int i = 0; i < Math.min(expectedLength, actualLength); i++) {
        assertEquals("Contents differ at byte: "+offsetCompared, expectedBuffer[i], actualBuffer[i]);
        offsetCompared++;
      }
      // Remove the compared bytes from the buffers
      if (expectedLength < actualLength) {
        System.arraycopy(actualBuffer, expectedLength, actualBuffer, 0, actualLength - expectedLength);
        actualLength -= expectedLength;
        expectedLength = 0;
      } else {
        System.arraycopy(expectedBuffer, actualLength, expectedBuffer, 0, expectedLength - actualLength);
        expectedLength -= actualLength;
        actualLength = 0;
      }
    }
  }

  /**
   * Tests two arrays for equality
   * @param expected the expected array
   * @param actual the actual array produced by the code
   */
  public static void assertArrayEquals(Object expected, Object actual) {
    int expectedLength = Array.getLength(expected);
    int actualLength = Array.getLength(actual);
    assertEquals(String.format("Array lengths differ. Expected %d but found %d", expectedLength, actualLength),
        expectedLength, actualLength);
    for (int i = 0; i < expectedLength; i++) {
      Object expectedObject = Array.get(expected, i);
      Object actualObject = Array.get(actual, i);
      assertEquals(String.format("The arrays differ at position #%d. Expected '%s' but found '%s'", i,
          expectedObject.toString(), actualObject.toString()), expectedObject, actualObject);
    }
  }

  /**
   * Tests two images for equality (pixel-by-pixel)
   * @param expected the first image (typically, the value that is known to be correct)
   * @param actual the second image (typically, the value retrieved from the code)
   */
  public static void assertImageEquals(BufferedImage expected, BufferedImage actual) {
    assertImageEquals(null, expected, actual);
  }

  public static void assertImageEquals(String message, BufferedImage expected, BufferedImage actual) {
    /*
    try {
      javax.imageio.ImageIO.write(expected, "png", new File("expected.png"));
      javax.imageio.ImageIO.write(actual, "png", new File("actual.png"));
    } catch (IOException e) {
      e.printStackTrace();
    }
    */
    assertEquals(
        String.format("Width is not compatible: Expected %d and actual %d", expected.getWidth(), actual.getWidth()),
        expected.getWidth(), actual.getWidth());
    assertEquals(
        String.format("Height is not compatible: Expected %d and actual %d", expected.getHeight(), actual.getHeight()),
        expected.getHeight(), actual.getHeight());
    for (int x = 0; x < expected.getWidth(); x++) {
      for (int y = 0; y < expected.getHeight(); y++) {
        int expectedPixel = expected.getRGB(x, y);
        int actualPixel = actual.getRGB(x, y);
        assertEquals(String.format("%sPixels not equal at (%d,%d): Expected %s and actual %s",
            message == null? "" : message+"\n", x, y,
            new Color(expectedPixel, true), new Color(actualPixel, true)), expectedPixel, actualPixel);
      }
    }
  }

  /**
   * An assertion for the existence of a file (or a directory)
   * @param filePath
   */
  protected void assertFileExists(String filePath) {
    assertTrue(String.format("File '%s' does not exist", filePath), new File(filePath).exists());
  }

  /**
   * Read all non-empty files in the given directory as an array of strings, one for each line.
   * @param dir the directory to read
   * @return one string array that combines the contents of all files.
   */
  protected String[] readFilesInDirAsLines(File dir) throws IOException {
    List<String> lines = new ArrayList<String>();
    for (File file : dir.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File d, String name) {
        return !name.startsWith("_") && !name.startsWith(".");
      }
    })) {
      if (file.length() > 0) {
        String[] fileLines = readFile(file.getPath());
        for (String line : fileLines)
          lines.add(line);
      }
    }
    return lines.toArray(new String[lines.size()]);
  }
}
