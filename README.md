JHDF
====

JHDF is a pure Java library for reading and processing
[Hierarchical Data Format (HDF)](https://www.hdfgroup.org/)
which is mostly used for storing remote sensing data.
This makes it highly compatible and portable with any system that
supports Java. In addition, this library opens the files using the
FileSystem API from Hadoop. This allows it to read files directly
from HDFS, S3, or FTP. This library is compatible with Spark and Hadoop.